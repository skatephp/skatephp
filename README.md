**Folders structure**
```
|-config
|   bootstrap.php
|   routes.php
|-public
|   .htaccess
|   index.php
|-src
|---Controllers
|       BaseController.php
|---Templates
|   |---Layouts
|   |---Views
|-vendor
```

**public/index.php** 
```php
require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require '../config/bootstrap.php';
```
**config/bootstrap.php** 
```php
use Skate\Core\Router;
use Skate\Core\Request;
    
define('PUBLIC_DIR', 'public');
define('SRC_DIR', 'src');
define('TEMPLATES_DIR', 'Templates');
    
define('DS', DIRECTORY_SEPARATOR);
    
define('ROOT_PATH', dirname(__DIR__) . DS);
define('PUBLIC_PATH', ROOT_PATH . PUBLIC_DIR);
define('SRC_PATH', ROOT_PATH . SRC_DIR . DS);
define('TEMPLATES_PATH', SRC_PATH . TEMPLATES_DIR . DS);
    
set_include_path(get_include_path() . PATH_SEPARATOR . TEMPLATES_PATH);
    
$router = new Router(SRC_DIR . DS . 'Controllers' . DS);
require_once 'routes.php';
$router->dispatch(new Request());
```

**config/routes.php** 
```php
$router->add(new Route('/', 'Main', 'page'));
```
**src/Controllers/Main.php** 
```php
class Main extends BaseController
{
    public function page()
    {
        $this->template->render('main');
    }
}
```