<?php

if (!function_exists('debug')) {
    function debug($var, $isDie = false)
    {
        echo '<pre>';
        if (empty($var)) {
            var_dump($var);
        } else {
            print_r($var);
        }
        echo '</pre>';

        if ($isDie) {
            die();
        }
    }
}
