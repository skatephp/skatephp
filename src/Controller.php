<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 27.08.2017
 * Time: 11:09
 */

namespace Skate;

use Skate\Core\Request;
use Skate\Core\Router;
use Skate\Template\Template;

/**
 * @property-read Request $request
 * @property-read Router $router
 * @property-read Template $template
 */
class Controller
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Template
     */
    private $template = null;

    public function __get($name)
    {
        switch ($name) {
            case 'router':
                return $this->router;
                break;
            case 'request':
                return $this->request;
                break;
            case 'template':
                return $this->getTemplate();
                break;
            default:
                return null;
        }
    }

    /**
     * @param string $view
     * @param array $data
     */
    public function render(string $view, array $data = [])
    {
        $this->getTemplate()->render($view, $data);
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    private function getTemplate()
    {
        if (!($this->template instanceof Template)) {
            $this->template = new Template($this);
        }

        return $this->template;
    }
}
