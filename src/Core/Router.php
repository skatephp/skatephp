<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 20.08.2017
 * Time: 13:44
 */

namespace Skate\Core;

use Skate\Controller;

class Router
{
    private $controllersPath = '';

    private $routes = [];

    /**
     * @var Route
     */
    private $currentRoute = null;

    private $arguments = [];

    public function __construct($controllersPath)
    {
        $this->controllersPath = $controllersPath;
    }

    /**
     * @param Route $route
     */
    public function add(Route $route)
    {
        $this->routes[] = $route;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function match($request): bool
    {
        $url = $request->path();
        if (empty($url)) {
            $url = '/';
        }

        foreach ($this->routes as $route) {
            /**
             * @var Route $route
             */
            if (preg_match('#^' . $route->getPattern() . '$#i', $url, $matches)) {
                /**
                 * @var Route $route
                 */
                if ($route->getMethod() != Request::METHOD_ANY &&
                    strtoupper($route->getMethod()) != $request->method()) {
                        continue;
                }
                // Get all arguments
                foreach ($matches as $key => $match) {
                    if (is_integer($key)) {
                        unset($matches[$key]);
                        $this->arguments = $matches;
                    }
                }
                $this->currentRoute = $route;

                return true;
            }
        }

        return false;
    }

    /**
     * @param Request $request
     */
    public function dispatch(Request $request)
    {
        if ($this->match($request)) {
            $className = $this->controllersPath . $this->currentRoute->getController();

            if (!class_exists($className)) {
                // TODO: throw exception
                die('Class not found: ' . $className);
            }

            /**
             * @var Controller $classInstance
             */
            $classInstance = new $className;

            $classInstance->setRouter($this);
            $classInstance->setRequest($request);

            $action = $this->currentRoute->getAction();
            if (!method_exists($classInstance, $action)) {
                // TODO: throw exception
                die('Method not found: ' . $className . '::' . $action);
            }

            if (empty($this->arguments)) {
                $classInstance->$action();
            } else {
                call_user_func_array([$classInstance, $action], $this->arguments);
            }
        } else {
            // TODO: throw exception
            die('Error 404');
        }
    }

    /**
     * @return string
     */
    public function getControllersPath(): string
    {
        return $this->controllersPath;
    }
}
