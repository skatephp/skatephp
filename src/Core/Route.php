<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 23.09.2017
 * Time: 17:52
 */

namespace Skate\Core;

class Route
{
    private $pattern;
    private $controller;
    private $action;
    private $method;

    public function __construct(
        string $pattern,
        string $controller,
        string $action,
        string $method = Request::METHOD_ANY
    ) {
        $this->pattern = $pattern;
        $this->controller = $controller;
        $this->action = $action;
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
