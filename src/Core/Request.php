<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 20.08.2017
 * Time: 14:56
 */

namespace Skate\Core;

class Request
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
    const METHOD_ANY = 'ANY';

    public function scheme()
    {
        return $_SERVER['REQUEST_SCHEME'];
    }

    public function host()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public function port()
    {
        return $_SERVER['SERVER_PORT'];
    }

    public function path()
    {
        return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    }

    public function query()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    }

    public function fullUrl()
    {
        $fullUrl = $this->url();

        if ($queryParams = $this->query()) {
            $fullUrl .= '?' . $this->query();
        }

        return $fullUrl;
    }

    public function url()
    {
        $url = $this->scheme() . '://' . $this->host();

        if ($path = $this->path()) {
            $url .= '/' . $this->path();
        }

        return $url;
    }

    public function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function isPost()
    {
        return $this->method() === self::METHOD_POST;
    }

    public function isGet()
    {
        return $this->method() === self::METHOD_GET;
    }

    public function queryParam($paramName)
    {
        $queryParams = $this->queryParams();

        if (isset($queryParams[$paramName])) {
            return $queryParams[$paramName];
        } else {
            return null;
        }
    }

    public function queryParams()
    {
        $queryParams = [];
        $query = explode('&', $this->query());

        foreach ($query as $parameter) {
            $parametersArray = explode('=', $parameter);
            if (isset($parametersArray[0]) && isset($parametersArray[1])) {
                $queryParams[$parametersArray[0]] = $parametersArray[1];
            }
        }

        return $queryParams;
    }
}
