<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 15.09.2017
 * Time: 12:01
 */

namespace Skate\Template;

use Skate\Helpers\ArrayHelper;

class Renderer
{
    /**
     * @param string $text
     * @param array $data
     *
     * @return string
     */
    public function renderText(string $text, array $data): string
    {
        $data = (new ArrayHelper())->prepareDotValues($data);
        $text = preg_replace_callback(
            "'{{(.+?)}}'i",
            function ($m) use ($data) {
                return isset($data[$m[1]]) ? $data[$m[1]] : $m[0];
            },
            $text
        );

        return $text;
    }
}
