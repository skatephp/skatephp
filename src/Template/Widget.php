<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 24.09.2017
 * Time: 11:59
 */

namespace Skate\Template;

use Skate\Core\Request;

/**
 * @property-read Request $request
 */
class Widget
{
    /**
     * @var Template
     */
    private $template = null;

    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'request':
                return $this->template->getParentController()->request;
                break;
            default:
                return null;
        }
    }

    /**
     * @param string $widgetName
     * @param array $data
     */
    public function render(string $widgetName, array $data = [])
    {
        $this->template->renderWidget($widgetName, $data);
    }
}
