<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 23.09.2017
 * Time: 21:28
 */

namespace Skate\Template;

use Skate\Controller;

class Template
{
    const LAYOUTS_TEMPLATES_DIR = 'Layouts';
    const VIEWS_TEMPLATES_DIR = 'Views';
    const BLOCKS_TEMPLATES_DIR = 'Blocks';
    const WIDGETS_TEMPLATES_DIR = 'Widgets';
    const WIDGETS_CONTROLLERS_DIR = 'Widgets';
    const WIDGETS_DEFAULT_ACTION = 'run';

    /**
     * @var Controller
     */
    private $parentController;

    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var string
     */
    private $baseDir = '';

    /**
     * @var string
     */
    private $extension = '.php';

    /**
     * @var string
     */
    private $layout = 'index';

    /**
     * @var string
     */
    private $view = '';

    /**
     * @var array
     */
    private $data;

    public function __construct(Controller $parentController)
    {
        $this->parentController = $parentController;
        $this->renderer = new Renderer();
    }

    /**
     * @param string $viewName
     * @param array $data
     */
    public function render(string $viewName, array $data = [])
    {
        $this->view = $viewName;
        $content = $this->loadFile(self::VIEWS_TEMPLATES_DIR . DS . $viewName, $data);
        if ($layout = $this->getLayout()) {
            $data = ['content' => $content] + $data;
            $content = $this->loadFile(self::LAYOUTS_TEMPLATES_DIR . DS . $layout, $data);
        }

        ob_start();
        echo $content;
        ob_end_flush();
    }

    /**
     * @param string $widgetName
     * @param array $data
     */
    public function renderWidget(string $widgetName, array $data = [])
    {
        $content = $this->loadFile(self::WIDGETS_TEMPLATES_DIR . DS . $widgetName, $data);

        ob_start();
        echo $content;
        ob_end_flush();
    }

    /**
     * @param string $blockName
     * @param array $data
     */
    public function includeBlock(string $blockName, array $data = [])
    {
        $content = $this->loadFile(self::BLOCKS_TEMPLATES_DIR . DS . $blockName, $data);

        ob_start();
        echo $content;
        ob_end_flush();
    }

    /**
     * @param string $viewName
     * @param array $data
     */
    public function include(string $viewName, array $data = [])
    {
        if ($this->view != $viewName) {
            $content = $this->loadFile(self::VIEWS_TEMPLATES_DIR . DS . $viewName, $data);
            ob_start();
            echo $content;
            ob_end_flush();
        }
    }

    public function widget(string $widgetName, $action = self::WIDGETS_DEFAULT_ACTION, array $data = [])
    {
        $className = $this->getParentController()->router->getControllersPath() .
                     self::WIDGETS_CONTROLLERS_DIR . DS . $widgetName;
        if (!class_exists($className)) {
            // TODO: throw exception
            die('Class not found: ' . $className);
        }

        /**
         * @var Widget $classInstance
         */
        $classInstance = new $className($this);

        if (!method_exists($classInstance, $action)) {
            // TODO: throw exception
            die('Method not found: ' . $className . '::' . $action);
        }

        if (empty($data)) {
            $classInstance->$action();
        } else {
            call_user_func_array([$classInstance, $action], $data);
        }
    }

    private function loadFile(string $filename, array $data = []): string
    {
        $filename = $this->getBaseDir() . $filename . $this->getExtension();
        // TODO: check if file exist
        ob_start();
        $this->data = $data;
        /** @noinspection PhpIncludeInspection */
        require $filename;

        return ob_get_clean();
    }

    /**
     * @return Renderer
     */
    public function getRenderer(): Renderer
    {
        return $this->renderer;
    }

    /**
     * @return Controller
     */
    public function getParentController(): Controller
    {
        return $this->parentController;
    }

    /**
     * @return string
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     *
     * @return $this
     */
    public function setLayout(string $layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseDir(): string
    {
        return $this->baseDir;
    }

    /**
     * @param string $baseDir
     *
     * @return $this
     */
    public function setBaseDir(string $baseDir)
    {
        $this->baseDir = $baseDir;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $Extension
     *
     * @return $this
     */
    public function setExtension(string $Extension)
    {
        $this->extension = $Extension;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getDataValue(string $name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        } else {
            return '';
        }
    }
}
