<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 15.09.2017
 * Time: 12:06
 */

namespace Skate\Helpers;

class ArrayHelper
{
    const DOT_DELIMITER = '.';

    public function prepareDotValues($dataArray = [], $keyDotName = '')
    {
        $array = [];

        foreach ($dataArray as $key => $value) {
            $array[$keyDotName . $key] = $value;
            if (is_array($value)) {
                $array = array_merge($array, $this->prepareDotValues($value, $keyDotName . $key . self::DOT_DELIMITER));
            }
        }

        return $array;
    }
}
